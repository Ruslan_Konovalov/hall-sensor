void printLocalTime()
{ if (client.connected())
    {
     struct tm timeinfo;
     if(!getLocalTime(&timeinfo))
      {
     Serial.println("Failed to obtain time");
     return;
      }
    Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
    sec = timeinfo.tm_sec;
    hour = timeinfo.tm_hour;
    minute = timeinfo.tm_min;
    }
}
